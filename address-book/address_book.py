import json

class AddrBook:
    def __init__(self):
        self.token_map = dict()
        self.contacts = list()

    def update_token_map(self):
        self.token_map.clear()
        for contact in self.contacts:
            for t in contact.tokens:
                if t in self.token_map:
                    self.token_map[t].append(contact)
                else:
                    self.token_map[t] = [contact]


    def add_contact(self, contact):
        contact.update_tokens()
        self.contacts.append(contact)
        self.update_token_map()

    def delete_contact(self, contact):
        self.contacts.remove(contact)
        self.update_token_map()

    def retrieve_by_token(self, token):
        return self.token_map[token]

    def to_json(self):
        temp = [c.to_serializable_object() for c in self.contacts]
        return json.dumps(temp)
