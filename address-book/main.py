import contact
import address_book

Ngch = contact.Contact()
Ngch.name = "Ng Chun Hong"
Ngch.email = "chng3@e.ntu.edu.sg"
Ngch.phone = "98197480"
Ngch.address = "Blk 411, #09-752, Sembawang Drive"

Addr_Book = address_book.AddrBook()
Addr_Book.add_contact(Ngch)

ncy = contact.Contact()
ncy.name = "Ng Cheng Yee"
Addr_Book.add_contact(ncy)

print(Addr_Book.contacts)
print(Addr_Book.token_map)

for k, v in Addr_Book.token_map.items():
    for c in v:
        print(k + ": " + c.print_string())

for c in Addr_Book.retrieve_by_token("ng"):
    print(c.print_string())
