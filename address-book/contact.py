class Contact:
    def __init__(self):
        self.name = ""
        self.address = ""
        self.phone = ""
        self.email = ""
        self.tokens = {} # set datatype

    def update_tokens(self):
        temp = list()
        temp.extend(self.name.lower().split())
        temp.extend(self.address.lower().split())
        temp.append(self.phone)
        temp.append(self.email.lower())
        self.tokens = set(temp)

    def print_string(self):
        return self.name

    def to_serializable_object(self):
        temp = dict()
        temp["name"] = self.name
        temp["address"] = self.address
        temp["phone"] = self.phone
        temp["email"] = self.email
        
        return temp

